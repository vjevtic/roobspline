#include "RooSpline3Fun.h"

ClassImp(RooSpline3Fun);

RooSpline3Fun::RooSpline3Fun(const char* name, const char* title,
                             RooRealVar& var, const RooArgList& xpos,
                             const RooArgList& ypos)
    : RooAbsReal(name, title),
      x("x", "x", this, var),
      X("X", "Spline X positions", this),
      Y("Y", "Spline Y positions", this)
{
    // Check argument validity
    if (xpos.getSize() != ypos.getSize()) {
        std::cerr << "xpos and ypos need to have equal sizes!\n";
        exit(1);
    }
    if (xpos.getSize() < 4) {
        std::cerr << "Error: A minimum of 4 support points is required!\n";
        exit(1);
    }

    // Reserve memory -> size fixed during execution
    order = xpos.getSize();
    a  = std::vector<double>(order, 0.0);
    b  = std::vector<double>(order, 0.0);
    c  = std::vector<double>(order, 0.0);
    xs = std::vector<double>(order);
    ys = std::vector<double>(order);
    
    // Temporary storage for tridiagonal matrix diagonals
    D1  = std::vector<double>(order);
    D2  = std::vector<double>(order - 1);
    RHS = std::vector<double>(order);
    
    // Add parameters to proxy list
    for (int i = 0; i < order; ++i) {
        X.add(xpos[i]);
        Y.add(ypos[i]);
    }

    // Check spline point validity
    {
        auto itx = X.fwdIterator();
        RooAbsReal* c = nullptr;
        double val = -INFINITY;
        while ((c = static_cast<RooAbsReal*>(itx.next()))) {
            if (c->getValV() >= val) {
                val = c->getValV();
            }
            else {
            std::cerr << "Error: Spline x positions must increase monotonically\n";
                exit(1);
            }
        }
    }
    // Initialize splines
    update();
}

RooSpline3Fun::RooSpline3Fun(const RooSpline3Fun& other, const char* name)
    : RooAbsReal(other, name),
      x("x", this, other.x),
      X("X", this, other.X),
      Y("Y", this, other.Y),
      a(other.a), b(other.b),
      c(other.c), xs(other.xs),
      ys(other.ys), D1(other.D1),
      D2(other.D2), RHS(other.RHS),
      order(other.order)
{
    update();
}

void RooSpline3Fun::update() const
{
    // Load X and Y into handy access buffers xs and ys
    {
        auto itx = X.fwdIterator();
        auto ity = Y.fwdIterator();
        RooAbsReal* c = nullptr;
        int i = 0; 
        while ((c = static_cast<RooAbsReal*>(itx.next()))) {
            xs[i++] = c->getValV();
        }
        i = 0;
        while ((c = static_cast<RooAbsReal*>(ity.next()))) {
            ys[i++] = c->getValV();
        }
    }
    
    // Compute the main diagonal of the (symmetric) tridiagonal system
    for (int j = 2; j < order; ++j) {
        D1[j-2] = 2.0*(xs[j] - xs[j-2]);
    }

    // Compute the secondary diagonal of the (symmetric) tridiagonal system
    for (int j = 2; j < order - 1; ++j) {
        D2[j-2] = xs[j] - xs[j-1];
    }
    
    // Compute right-hand-side of tridiagonal system
    for (int j = 1; j < order - 1; ++j) {
        RHS[j-1]  = 3.0*(ys[j+1] - ys[j])/(xs[j+1] - xs[j]);
        RHS[j-1] -= 3.0*(ys[j] - ys[j-1])/(xs[j] - xs[j-1]);
    }

    // Solve tridiagonal system via Thomas algorithm
    // 1: update off-diagonal 
    std::vector<double> Dprime(order - 1, 0);
    Dprime[0] = D2[0]/D1[0];
    for (int j = 1; j < order - 3; ++j) {
        Dprime[j] = D2[j]/(D1[j]-Dprime[j-1]*D2[j]);
    }
 
    // 2. Update RHS
    RHS[0] /= D1[0];
    for (int j = 1; j < order - 2; ++j) {
        RHS[j] = (RHS[j] - D2[j - 1]*RHS[j - 1]) / (D1[j] - D2[j-1]*Dprime[j-1]);
    }

    // 3. Solve system iteratively (from last to first spline)
    for (int j = order - 3; j >= -1; --j) {
        if (j >= 0) {
            b[j+1]  = RHS[j] - Dprime[j]*b[j+2];
        } // Otherwise b[0] == 0 is accessed which is a boundary condition.
        a[j+1]  = (b[j+2] - b[j+1]) / (3.0*(xs[j+2] - xs[j+1]));
        c[j+1]  = (ys[j+2] - ys[j+1]) / (xs[j+2] - xs[j+1]);
        c[j+1] -= (b[j+2] - b[j+1])*(xs[j+2] - xs[j+1])/3.0;
        c[j+1] -= b[j+1]*(xs[j+2] - xs[j+1]);
    }
}

double RooSpline3Fun::evaluate() const
{
    auto poly3 = [] (double x_, double a_, double b_,
                     double c_, double d_, double o_) {
        return a_*std::pow(x_ - o_, 3) + b_*std::pow(x_ - o_, 2) + c_*(x_ - o_) + d_;
    };
    
    // Support points might have changed. 
    // Compute parameters again
    update();

    // Find interpolating spline whose definition range contains x
    for (int i = 1; i < order; ++i) {
        if (x >= xs[i - 1] && x < xs[i]) {
            return poly3(x, a[i-1], b[i-1], c[i-1], ys[i-1], xs[i-1]);
        }
    }
    // Evaluating outside support point range.
    if (x < xs[0]) {
        return poly3(x, a[0], b[0], c[0], ys[0], xs[0]);
    }
    else {
        return poly3(x, a[order-2], b[order-2], c[order-2], ys[order-2], xs[order-2]);
    }
}
