import numpy as np
import matplotlib.pyplot as plt


class Spline:
    def __init__(self, xvals, yvals):
        assert len(xvals) == len(yvals)
        self.X = xvals
        self.Y = yvals

        self.N = len(self.X)

        self.a = list(np.zeros(self.N))
        self.b = list(np.zeros(self.N))
        self.c = list(np.zeros(self.N))

    def eval(self, x0):
        N = -1
        for i, P in enumerate(self.X[:-1]):
            if x0 >= self.X[i] and x0 <= self.X[i + 1]:
                return self.a[i]*(x0 - P)**3 + self.b[i]*(x0 - P)**2 + self.c[i]*(x0 - P) + self.Y[i]
        if x0 < self.X[0]:
            return self.a[0]*(x0 - self.X[0])**3 + self.b[0]*(x0 - self.X[0])**2 + self.c[0]*(x0 - self.X[0]) + self.Y[0]
        else:
            return self.a[N-1]*(x0 - self.X[N-1])**3 + self.b[N-1]*(x0 - self.X[N-1])**2 + self.c[N-1]*(x0 - self.X[N-1]) + self.Y[N-1]

    def solve(self):
        # Thomas Algorithm
        X = self.X
        Y = self.Y
        N = self.N - 2 # Parameters b for i = 0 and i = N are set to zero

        a = self.a
        b = self.b
        c = self.c

        Cs = [X[i] - X[i - 1] for i in range(2, N + 1)]
        Bs = [2*(X[i] - X[i - 2]) for i in range(2, N + 2)]
        Ds = [3*((Y[i+1]-Y[i])/(X[i+1]-X[i]) - (Y[i]-Y[i-1])/(X[i]-X[i-1])) for i in range(1, N + 1)]

        print("X", X)
        print("Bs", Bs)
        print("Cs", Cs)
        print("Ds", Ds)

        # Update matrix diagonal
        C2s = [Cs[0]/Bs[0]]
        for i in range(1, self.N - 3):
            C2s.append(Cs[i]/(Bs[i]-C2s[-1]*Cs[i]))
        print("C2s", C2s)

        # Update RHS
        D2s = [Ds[0]/Bs[0]]
        for i in range(1, self.N - 2):
            D2s.append((Ds[i] - Cs[i-1]*D2s[i-1])/(Bs[i] - Cs[i-1]*C2s[i-1]))

        print("D2s", D2s)
       
        #b[-2] = D2s[-1]
        #a[-2] = (b[-1] - b[-2]) / (3*(X[-1]-X[-2]))
        #
        #c[-2] = (Y[-1] - Y[-2]) / (X[-1]-X[-2])
        #c[-2] -= (b[-1]-b[-2])*(X[-1]-X[-2])/3 
        #c[-2] -= b[-2]*(X[-1]-X[-2])
        print('Last spline', a[-2], b[-2], c[-2])
        print("a", a)
        print("b", b)
        print("c", c)
        
        b[-1] = 0
        a[-1] = -b[-2]/(3*(X[-1]))
        c[-1] = 0

        for i, d in list(reversed(list(enumerate(D2s)))):
            R = i+1
            print(i)
            print("d", d)
            print("C2s[i-1]", C2s[i-1])
            print("b[R+1]",b[R+1])
            print("b[R]",b[R])
            print("X[R+1]",X[R+1])
            print("X[R]",X[R])
            print("Y[R+1]",Y[R+1])
            print("Y[R]",Y[R])

            b[R] = d - C2s[i-1]*b[R+1]
            a[R] = (b[R+1] - b[R]) / (3*(X[R+1]-X[R]))
            c[R] = (Y[R+1] - Y[R]) / (X[R+1]-X[R])
            c[R] -= (b[R+1]-b[R])*(X[R+1]-X[R])/3 
            c[R] -= b[R]*(X[R+1]-X[R])
            print(a[R], b[R], c[R])

        b[0] = 0
        a[0] = b[1] / (3*(X[1]-X[0]))
        c[0] = (Y[1]-Y[0]) / (X[1]-X[0]) - b[1]*(X[1]-X[0])/3
        print("a", a)
        print("b", b)
        print("c", c)




x = np.linspace(0, 3, 1000)
xx = [0, 1, 2, 3] 
yy = [5, 6, -1, 2]

spline = Spline(xx, yy)
spline.solve()

res = [spline.eval(x0) for x0 in x]

plt.plot(x, res, '-')
plt.plot(xx, yy, 'rx')
plt.show()
