import ROOT as R
import numpy as np
from ROOT import RooFit, gSystem, gInterpreter
gInterpreter.ProcessLine("#include \"../RooBSplinePdf.h\"")
gInterpreter.ProcessLine("#include \"../RooBSplineFun.h\"")
gSystem.Load("../spline.so")

xx = [-0.5, 0, 0.5, 1, 1.5, 2.0, 2.5]
var = R.RooRealVar("var", "var", xx[0], xx[-1])
yy  = [1, 1, 1, 5, 1, 1, 1] 

X = R.RooArgList()
Y = R.RooArgList()

for i, x in enumerate(xx):
    varx = R.RooRealVar("x" + str(i), "",x, x-0.1, x+0.1) 
    R.SetOwnership(varx, False)
    X.add(varx)
    
    vary = R.RooRealVar("y" + str(i), "", yy[i], 0, 20)
    R.SetOwnership(vary, False)
    Y.add(vary)

fun = R.RooBSplinePdf("fun", "fun", var, X, Y)

# Generate gauss data
fun.Print('v')
gauss1 = R.RooGaussian("gauss1", "", var, R.RooFit.RooConst(1), R.RooFit.RooConst(0.5))
gauss2 = R.RooGaussian("gauss2", "", var, R.RooFit.RooConst(1.5), R.RooFit.RooConst(0.3))
frac = R.RooRealVar("frac", "", 0.5, 0, 1)
gauss = R.RooAddPdf("Gauss", "", R.RooArgList(gauss1, gauss2), R.RooArgList(frac))
data = gauss.generate(R.RooArgSet(var), 10000)
#datahist = R.RooDataHist("datahist", "", R.RooArgSet(var), data)
#fun.chi2FitTo(datahist)
fun.fitTo(data)

can = R.TCanvas("can", "", 800, 600)
frame = var.frame()

data.plotOn(frame)

fun.plotOn(frame, RooFit.Precision(1e-5))
frame.Draw()
can.Draw()
can.SaveAs("out.png")

