import ROOT as R
import numpy as np
from ROOT import RooFit, gSystem, gInterpreter
gInterpreter.ProcessLine("#include \"RooSpline3Pdf.h\"")
gInterpreter.ProcessLine("#include \"RooSpline3Fun.h\"")
gSystem.Load("spline.so")


xx = [0, 1, 2, 3]
var = R.RooRealVar("var", "var", xx[0], xx[-1])
yy  = np.random.uniform(1, 10, 4)

X = R.RooArgList()
Y = R.RooArgList()

for i, x in enumerate(xx):
    varx = R.RooRealVar("x" + str(i), "", x)
    R.SetOwnership(varx, False)
    X.add(varx)
    
    vary = R.RooRealVar("y" + str(i), "", yy[i])
    R.SetOwnership(vary, False)
    Y.add(vary)

fun = R.RooSpline3Pdf("fun", "fun", var, X, Y)

can = R.TCanvas("can", "", 800, 600)
frame = var.frame()
fun.plotOn(frame, RooFit.Precision(1e-5))
frame.Draw()
can.Draw()
can.SaveAs("out.png")

