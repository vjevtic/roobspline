#ifndef ROOSPLINE3PDF_H
#define ROOSPLINE3PDF_H

#include <iostream>
#include <vector>
#include <cmath>

#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooAbsArg.h"
#include "RooRealProxy.h"
#include "RooListProxy.h"
#include "RooAbsReal.h"
#include "RooArgList.h"

// Author: Vukan Jevtic <vukan.jevtic@cern.ch>

/// Cubic Spline PDF Class
///
/// Given a list of support points xpos and values ypos, RooSpline3Pdf builds a 
/// PDF going through all specified points. Since RooSpline3Pdf is a probability
/// density, the y positions can be scaled automatically in order to normalize the 
/// interpolated curve to a constant. Spline y positions are required to be > 0.
/// A minimum of 4 support points is required.
/// If RooSpline3Pdf is evaluated outside of the range defined by the first and last 
/// support point the first or last spline is evaluated respectively as an extrapolation
///
/// Note: The interpolated 3rd degree polynomial could still be < 0 in some 
/// interval between two support points. Since this is very likely happening for 
/// certain scenarios, RooSpline3Pdf returns the value 1e-15 in this case to 
/// suppress additional RooFit warnings. Make sure to inspect the fit result visually.

class RooSpline3Pdf : public RooAbsPdf {
    public:
        RooSpline3Pdf() = default;
        RooSpline3Pdf(const char* name, const char* title, RooRealVar& x, 
                      const RooArgList& xpos, const RooArgList& ypos);
        RooSpline3Pdf(const RooSpline3Pdf&, const char* name=nullptr);

        virtual TObject* clone(const char* newname) const { return new RooSpline3Pdf(*this, newname); }
        inline virtual ~RooSpline3Pdf() = default;

        double evaluate(double);

    protected:
        double evaluate() const;
        void update() const; // Computes spline parameters
        
        // nth spline = a_n*(x-X_n)^3 + b_n*(x-X_n)^2 + c_n*(x-X_n) + Y_n
        RooRealProxy x;
        RooListProxy X, Y;

        mutable std::vector<double> a, b, c, xs, ys;
        mutable std::vector<double> D1, D2, RHS; // Matrix diagonals and right-hand-side

        // order == number of support points provided. #Splines = order - 1
        int order;

    private:
        ClassDef(RooSpline3Pdf, 1)
};

#endif // ROOSPLINE3PDF_H
