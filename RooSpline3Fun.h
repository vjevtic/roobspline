#ifndef ROOSPLINE3FUN_H
#define ROOSPLINE3FUN_H

#include <iostream>
#include <vector>
#include <cmath>

#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooAbsArg.h"
#include "RooRealProxy.h"
#include "RooListProxy.h"
#include "RooAbsReal.h"
#include "RooArgList.h"

// Author: Vukan Jevtic <vukan.jevtic@cern.ch>

/// Cubic Spline PDF Class
///
/// Given a list of support points xpos and values ypos, RooSpline3Fun builds a 
/// function going through all specified points. A minimum of 4 support points is required.
/// If RooSpline3Fun is evaluated outside of the range defined by the first and last 
/// support point the first or last spline is evaluated respectively as an extrapolation.

class RooSpline3Fun : public RooAbsReal {
    public:
        RooSpline3Fun() = default;
        RooSpline3Fun(const char* name, const char* title, RooRealVar& x, 
                      const RooArgList& xpos, const RooArgList& ypos);
        RooSpline3Fun(const RooSpline3Fun&, const char* name=nullptr);

        virtual TObject* clone(const char* newname) const { return new RooSpline3Fun(*this, newname); }
        inline virtual ~RooSpline3Fun() = default;

        double evaluate(double);

    protected:
        double evaluate() const;
        void update() const; // Computes spline parameters
        
        // nth spline = a_n*(x-X_n)^3 + b_n*(x-X_n)^2 + c_n*(x-X_n) + Y_n
        RooRealProxy x;
        RooListProxy X, Y;

        mutable std::vector<double> a, b, c, xs, ys;
        mutable std::vector<double> D1, D2, RHS; // Matrix diagonals and right-hand-side

        // order == number of support points provided. #Splines = order - 1
        int order;

    private:
        ClassDef(RooSpline3Fun, 1)
};

#endif // ROOSPLINE3FUN_H
