# compiler
CC=g++

# common use flags and libraries
CXXFLAGS := -Wall -Wextra  -Woverloaded-virtual -fPIC -W -pipe 
CXXFLAGS += -ftree-vectorize #-march=native  
CXXFLAGS +=  #-Wabi -Weffc++ -Wno-reorder
OPTIMISE  :=  -O3
DEBUG     := -ggdb -v 
ROOTLIBS  :=  $(shell root-config --libs) -lRooFit -lRooStats -lTreePlayer
ROOTFLAGS :=  $(shell root-config --cflags)
INCROOT   :=  -I$(shell root-config --incdir)
CXXFLAGS  += $(ROOTFLAGS)
CXXFLAGS  += $(ROOTLIBS)
LDFLAGS    = -O2 # -Wl
SOFLAGS    = -shared
SHLIB    := spline.so
HDRS     := $(wildcard *.h)
COMPILE = $(CC) $(CXXFLAGS) -c

OBJFILES := $(patsubst %.cxx,%.o,$(wildcard *.cxx))
OBJFILES += eventdict.o

default : $(SHLIB)
	#$(CC) $(CXXFLAGS) $(DEBUG) test.cpp -o test -I. -L. -l Kll

%.o: %.cxx
	$(CC) $(CXXFLAGS) $(DEBUG) -c $< -o $@

$(SHLIB): $(OBJFILES) $(INTS) $(HDRS)
	  @echo "Linking $(SHLIB) ..."
	  /bin/rm -f $(SHLIB)
	  $(CC) $(SOFLAGS) $(LDFLAGS) $(OBJFILES) -o $(SHLIB) 
	  @echo "done"

eventdict.o:  $(HDRS)
	@echo "Generating dictionary ..."
	@echo "rootcint -f eventdict.cc -c $(HDRS)"
	@rootcint -f eventdict.cc -c $(HDRS)
	$(COMPILE) eventdict.cc -I. -o eventdict.o 

clean: 	
	rm -f $(SHLIB) $(OBJFILES) eventdict.* 

